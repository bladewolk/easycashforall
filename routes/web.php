<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{first?}/{seconds?}', function () {
    return view('welcome');
});

//Auth::routes();
//Route::get('logout', 'Auth\LoginController@logout');
//
//Route::prefix('dashboard')->namespace('Dashboard')->middleware('auth')->group(function () {
//    Route::get('/', 'HomeController@index')->name('dashboard');
//    Route::resource('tasks', 'TaskController');
//    Route::get('/users', 'UserController@index')->name('users');
//});
//
//
////Tests Controller
//Route::get('webmoney', 'Api\PaymentController@index');



