<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('v1/login', 'Api\Auth\LoginController@login');

Route::middleware('auth:api')->group(function () {
    Route::post('v1/inviteCode', 'Api\Auth\LoginController@inviteCode');

    Route::namespace('Api\v1')->prefix('v1')->group(function () {
        Route::get('test', 'HomeController@index');
        Route::get('tasks', 'TaskController@index');
        Route::patch('tasks/{id}', 'TaskController@update');

        Route::post('payment', 'PaymentController@index');
        Route::post('profile', 'ProfileController@index');
    });
});


Route::post('v2/login', 'Api\v2\AuthController@login');
Route::post('v2/register', 'Api\v2\AuthController@register');


Route::get('v2/tasks', 'Api\v2\TaskController@index');
Route::post('v2/tasks/checkPackage', 'Api\v2\TaskController@checkPackage');
Route::post('v2/tasks/checkDescription', 'Api\v2\TaskController@checkDescription');
Route::post('v2/tasks/store', 'Api\v2\TaskController@store');

Route::get('v2/users', 'Api\v2\UserController@index');
Route::delete('v2/users', 'Api\v2\UserController@destroy');


