<?php

namespace App\Http\Controllers\Api\v2;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $role = $request->get('role');
        $search = $request->get('search', null);

        $users = User::withCount('referals');

        if ($search)
            $users = $users->where('email', 'like', $search . '%');

        if ($role)
            $users = $users->whereHas('roles', function ($query) use ($role) {
                $query->where('name', $role);
            });


        return $users->paginate(12);
    }

    public function destroy()
    {
        User::whereIn('id', request()->get('ids'))->each(function ($user) {
            $user->delete();
        });

        return request()->get('ids');
    }
}