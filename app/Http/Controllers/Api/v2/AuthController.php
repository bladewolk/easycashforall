<?php

namespace App\Http\Controllers\Api\v2;


use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        $this->validate(request(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

//        If not pass
        if (!Auth::attempt(request()->only('email', 'password')))
            return response([
                'errors' => [
                    'another' => ['Credentials not found!']
                ]
            ], 422);

        $user = User::whereEmail(request()->get('email'))->first();


        return [
            'access_token' => 'Bearer ' . $user->createToken('Bearer')->accessToken,
        ];
    }

    public function register()
    {
        $this->validate(request(), [
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::create([
            'name' => str_random(8),
            'email' => request()->get('email'),
            'password' => bcrypt(request()->get('password'))
        ]);

        $user->roles()->attach(Roles::whereName(User::ADMIN)->first());

        return response([
            'access_token' => 'Bearer ' . $user->createToken('Bearer')->accessToken,
        ]);
    }
}