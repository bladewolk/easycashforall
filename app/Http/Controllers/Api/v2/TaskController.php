<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Exception;
use Illuminate\Http\Request;
use Raulr\GooglePlayScraper\Scraper;


class TaskController extends Controller
{
    public function checkPackage()
    {
        $this->validate(request(), [
            'package' => 'required'
        ]);

        try {
            $scrapper = new Scraper();
            $res = $scrapper->getApp(request()->get('package'));
        } catch (Exception $e) {
            return response([
                'errors' => [
                    'package' => ['Invalid package name']
                ]
            ], 422);
        }
    }

    public function checkDescription()
    {
        $this->validate(request(), [
            'stars' => 'required|min:1|max:3',
            'reward' => 'required',
            'expiry_date' => 'required|after:today'
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'package' => 'required',
            'stars' => 'required|min:1|max:3',
            'reward' => 'required',
            'expiry_date' => 'required|after:today',
            'conditions' => 'array',
            'conditions.*.type' => 'required',
            'conditions.*.text' => 'required',
            'conditions.*.repeating_count' => 'required_if:conditions.*.type,REPEATER'
        ], [
            'conditions.*.text.required' => 'The condition field required!',
            'conditions.*.repeating_count.required_if' => 'Required when selected type Repeater'
        ]);

        $scrapper = new Scraper();
        $res = $scrapper->getApp($request->get('package'));

        $task = new Task();
        $task->fill($request->only(
            'package',
            'expiry_date',
            'stars',
            'reward'
        ));
        $task->fill($res);
        $task->save();

        $task->conditions()->createMany($request->conditions);
    }

    public function index()
    {
        $tasks = Task::latest()
            ->with('conditions')
            ->get()
            ->makeHidden('status');

        return $tasks;
    }
}