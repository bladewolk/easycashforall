<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/v1/login",
     *     summary="Login route",
     *     tags={"Auth"},
     *     description="Login route",
     *     operationId="Login",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="name", in="query", description="User name", required=true, type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="email", in="query", description="User name", required=true, type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *          @SWG\Property(type="object",title="Response ",
     *              @SWG\Property(property="token", type="string", description="Bearer token to api authorization, paste into
    header Authorization"),
     *              @SWG\Property(property="promocode", type="boolean", description="Your promocode for invite your friends"),
     *              @SWG\Property(property="entered_promo", type="boolean", description="Invite code,
    If true -> you already enter invite_code, false -> you do not enter invite_code"),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response="403",
     *         description="Validation error",
     *         @SWG\Schema(ref="#/definitions/validation error")
     *     ),
     * )
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()->all()
            ], 403);
        }

        $user = User::whereEmail($request->input('email'))->first();

        if ($user === null) {
            $input = $request->only('name', 'email');
            $input['promocode'] = $this->generatePromo();

            $user = User::create($input);
            $user->roles()->attach(Roles::whereName(User::USER)->first());

            return response([
                'token' => 'Bearer ' . $user->createToken('Token')->accessToken,
                'promocode' => $user->promocode,
                'entered_promo' => false
            ], 200);
        }

        $token = $user->createToken('Token')->accessToken;

        return response([
            'token' => "Bearer $token",
            'promocode' => $user->promocode,
            'entered_promo' => $user->invite_code ? true : false
        ], 200);
    }

    private function generatePromo(): string
    {
        do {
            $code = strtoupper(str_random(4) . '-' . str_random(4));
            $alreadyExist = User::wherePromocode($code)->exists();
        } while ($alreadyExist);

        return $code;
    }

    /**
     * @SWG\Post(
     *     path="/v1/inviteCode",
     *     summary="Enter invite code",
     *     tags={"Auth"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="invite_code", required=true, in="query", description="Invite code", type="string"),
     *     @SWG\Response(
     *         response=200,
     *         description="Response successfull",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="success", type="boolean", example="true"),
     *              @SWG\Property(property="message", type="string", example="You enter wrong promocode!"),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         @SWG\Property(ref="#/definitions/Unauthenticated"),
     *         description="Unauthenticated",
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     * @param Request $request
     * @return array
     */
    public function inviteCode(Request $request)
    {
        $inviteCode = $request->input('invite_code');
        $check = User::wherePromocode($inviteCode)->first();
        $user = auth('api')->user();

        if ($user->invite_code)
            return response([
                'success' => false,
                'message' => 'You already enter promocode!'
            ]);

        if ($check) {
            $check->referals()->attach($user);
            $user->update([
                'invite_code' => $inviteCode
            ]);

            return response([
                'success' => $check,
                'message' => 'You enter correct promocode'
            ]);
        }
        return response([
            'success' => $check,
            'message' => 'You enter wrong promocode!'
        ]);
    }
}
