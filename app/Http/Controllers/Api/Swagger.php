<?php

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     basePath="/api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Easy Cash For All",
 *         description="Api for EasyCashForAll App",
 *         ),
 *     @SWG\SecurityScheme(
 *         securityDefinition="Bearer",
 *         type="apiKey",
 *         name="Authorization",
 *         in="header"
 *     ),
 *         @SWG\Definition(
 *              definition="Unauthenticated",
 *               @SWG\Property(type="array", property="errors",
 *                  @SWG\Items(type="string", example="Unauthenticated"),
 *              ),
 *         ),
 *         @SWG\Definition(
 *              definition="Server error",
 *               @SWG\Property(type="array", property="errors",
 *                  @SWG\Items(type="string", example="Can not find model!"),
 *              ),
 *         ),
 *         @SWG\Definition(
 *              definition="validation error",
 *               @SWG\Property(type="array", property="errors",
 *                  @SWG\Items(type="string", example="The email must be a valid email address."),
 *              ),
 *         ),
 *         @SWG\Definition(
 *              definition="User model",
 *               @SWG\Property(property="id", type="integer", description="User ID"),
 *               @SWG\Property(property="name", type="string", example="Testusername"),
 *               @SWG\Property(property="promocode", type="string", description="Invite your frind with this code", example="AXML-4ASD"),
 *               @SWG\Property(property="balance", type="float", description="Current user balance", example="1.05"),
 *               @SWG\Property(property="completed_tasks", type="integer", description="The count of completed tasks", example="3"),
 *               @SWG\Property(property="referals", type="integer", description="The count user referals"),
 *         ),
 *         @SWG\Definition(
 *              definition="Task model",
 *               @SWG\Property(property="id", type="integer", description="Use this ID in request when you apply task"),
 *               @SWG\Property(property="package", type="string", description="App package name", example="com.arndroid.com"),
 *               @SWG\Property(property="image", type="string", description="Link to app image"),
 *               @SWG\Property(property="title", type="string", description="Task name, become from playmarket App name"),
 *               @SWG\Property(property="stars", type="integer", description="This task stars", example="3"),
 *               @SWG\Property(property="available", type="boolean", description="Ligic if user can update this task", example="true"),
 *               @SWG\Property(property="timeRemaining", type="string", description="Generating on server time that
app can be allowed. Use in one android screen", example="1д 2ч 1м"),
 *               @SWG\Property(property="status", type="string", description="Status of current task for this user.
Available statuses in example", example="NEW | IN_PROGRESS | COMPLETED | CANCELED"),
 *          ),
 *         @SWG\Definition(
 *              definition="Task condition",
 *               @SWG\Property(property="text", type="string", description="Some text of current task. "),
 *         ),
 *         @SWG\Definition(
 *              definition="Task model with conditions",
 *               @SWG\Property(property="id", type="integer", description="Use this ID in request when you apply task"),
 *               @SWG\Property(property="package", type="string", description="App package name", example="com.arndroid.com"),
 *               @SWG\Property(property="image", type="string", description="Link to app image"),
 *               @SWG\Property(property="title", type="string", description="Task name, become from playmarket App name"),
 *               @SWG\Property(property="available", type="boolean", description="Ligic if user can update this task", example="true"),
 *               @SWG\Property(property="stars", type="integer", description="This task stars", example="3"),
 *               @SWG\Property(property="timeRemaining", type="string", description="Generating on server time that
app can be allowed. Use in one android screen", example="1д 2ч 1м"),
 *               @SWG\Property(property="status", type="string", description="Status of current task for this user.
Available statuses in example", example="NEW | IN_PROGRESS | COMPLETED"),
 *              @SWG\Property(
 *                  property="conditions", type="array",
 *                  @SWG\Items(ref="#/definitions/Task condition"),
 *              ),
 *          ),
 *         @SWG\Definition(
 *              definition="TASK Status",
 *               @SWG\Property(property="NEW", type="string", description="New available task, you can accept this!"),
 *               @SWG\Property(property="IN_PROGRESS", type="string", description="You apply this task. Current task in progress. Some
conditions are available"),
 *              @SWG\Property(property="COMPLETED", type="string", description="You fully complete this task. All conditions finished!"),
 *              @SWG\Property(property="REPEATER_WAITING", type="string", description="This task have condition REPEATER
that you once complete and 24 hours did not pass!"),
 *         ),
 *         @SWG\Definition(
 *              definition="TASK Conditions type",
 *               @SWG\Property(property="ONE_TIME_INSTALL", type="string", description="One time step.", example="Install app"),
 *               @SWG\Property(property="ONE_TIME", type="string", description="One time step. Random complete",
example="Make comment"),
 *               @SWG\Property(property="REPEATER", type="string", description="Repeating step 1/24 hours",
example="Repeater", example="Open app 3 days every 24hours"),
 *         ),
 *         @SWG\Definition(
 *              definition="TASK Conditions status",
 *               @SWG\Property(property="DONE", type="string", description="You complete task condition"),
 *               @SWG\Property(property="AVAILABLE", type="string", description="You can complete this step"),
 *               @SWG\Property(property="NOT_AVAILABLE", type="string", description="This condition is REPEATER and now it's not available"),
 *         ),
 * )
 */