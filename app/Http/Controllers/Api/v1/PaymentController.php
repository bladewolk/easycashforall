<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\QiwiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/v1/payment",
     *     summary="Transfer money",
     *     tags={"Balance"},
     *     description="Transfer types: QIWI, PayPalPayment, WebMoney",
     *     produces={"application/json"},
     * @SWG\Parameter(
     *     required=true,
     *   name="payment",
     *   in="query",
     *   type="string",
     *   enum={"QIWI", "YANDEX_MONEY", "WEBMONEY", "PAYPAL"}
     * ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operationss",
     *          @SWG\Schema(
     *              @SWG\Property(property="user", ref="#/definitions/User model"),
     *              @SWG\Property(
     *                  type="array",
     *                  property="tasks",
     *                  @SWG\Items(
     *                      ref="#/definitions/Task model with conditions",
     *                  ),
     *              ),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @SWG\Property(ref="#/definitions/Unauthenticated"),
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function index(Request $request)
    {
        if ($request->input('type') === 'QIWI')
            return $this->qiwi();

        return 123;
    }

    private function qiwi()
    {
//        TODO complete this method
        $qiwi = new QiwiHelper();
        return response([
            'result' => $qiwi->getBalance()
        ]);
    }
}
