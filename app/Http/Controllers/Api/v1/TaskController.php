<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\TaskHelper;
use App\Models\Task;
use App\models\TaskCondition;
use App\models\UserTask;
use App\Models\UserTaskCondition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class TaskController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = auth('api')->user();
    }

    /**
     * @SWG\Get(
     *     path="/v1/tasks",
     *     summary="Get Tasks",
     *     tags={"Tasks"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operationss",
     *          @SWG\Schema(
     *              @SWG\Property(property="balance", type="float", description="Current user balance", example=0.01),
     *              @SWG\Property(
     *                  type="array",
     *                  property="tasks",
     *                  @SWG\Items(
     *                      ref="#/definitions/Task model with conditions",
     *                  ),
     *              ),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @SWG\Property(ref="#/definitions/Unauthenticated"),
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function index()
    {
        $tasks = Task::with('conditions')
            ->latest()
            ->take(40)
            ->get();

        return [
            'balance' => $this->user->balance,
            'tasks' => TaskHelper::getAndModifyTasks($tasks)
        ];
    }

    /**
     * @SWG\Patch(
     *     path="/v1/tasks/{id}",
     *     summary="Update task",
     *     tags={"Tasks"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="id", required=true, in="path", description="task ID", type="integer"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operationss",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="success", type="boolean", example="true"),
     *              @SWG\Property(property="task_status", type="string", example="IN_PROGRESS | COMPLETED"),
     *              @SWG\Property(property="balance", type="float", description="User balance, update this if task = COMPLETED",
     example=0.03),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @SWG\Property(ref="#/definitions/Unauthenticated"),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Error on server",
     *         @SWG\Property(ref="#/definitions/Server error"),
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        $task = $this->user->tasks()->wherePivot('task_id', $id)->first();

        if ($task == null) {
            $exitTask = Task::find($id);
            if ($exitTask == null)
                return response([
                    'errors' => [
                        "Can't Find Model!"
                    ]
                ], 404);

            $this->user->tasks()->attach($exitTask, [
                'status' => UserTask::IN_PROGRESS
            ]);

            $task = $this->user->tasks()->wherePivot('task_id', $id)->first();
            $taskConditionsIDs = $exitTask->conditions()->pluck('id');
            $this->user->taskConditions()->attach($taskConditionsIDs, [
                'status' => UserTaskCondition::AVAILABLE
            ]);
        }

        $conditions = $this->user->taskConditions()->where('task_id', $id)->get();

        $conditions = TaskHelper::completeConditions($conditions);
        $task = TaskHelper::updateGlobalTaskStatus($task, $conditions);

        return response([
            'success' => true,
            'task_status' => $task->status,
            'balance' => $this->user->balance
        ]);
    }
}
