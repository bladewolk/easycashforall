<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth('api')->user();
    }

    /**
     * @SWG\Post(
     *     path="/v1/profile",
     *     summary="Get Profile data",
     *     tags={"Profile"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operationss",
     *          @SWG\Schema(
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="email", type="string"),
     *              @SWG\Property(property="promocode", type="string"),
     *              @SWG\Property(property="balance", type="float", example=0.05),
     *              @SWG\Property(property="referals_count", type="integer", example=2),
     *              @SWG\Property(property="referals", type="array",
     *                  @SWG\Items(type="string", description="emails"),
     *              ),
     *              @SWG\Property(property="completed_tasks", type="array",
     *                  @SWG\Items(type="object",
     *                      @SWG\Property(property="package", type="string"),
     *                      @SWG\Property(property="image", type="string"),
     *                      @SWG\Property(property="title", type="string"),
     *                      @SWG\Property(property="reward", type="float", example=0.05),
     *                      @SWG\Property(property="stars", type="integer", example=3),
     *                      @SWG\Property(property="status", type="string", example="COMPLETED"),
     *                  ),
     *              ),
     *              @SWG\Property(property="completed_tasks_count", type="integer", example=3),
     *              @SWG\Property(property="referal_income", type="float", example=0.43, description="Income from referals"),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @SWG\Property(ref="#/definitions/Unauthenticated"),
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function index()
    {
        $user = User::withCount('referals')
            ->find($this->user->id)
            ->makeHidden(['created_at', 'updated_at']);

        $user->referals = $user->referals()->pluck('email');
        $user->completed_tasks = $user->completedTasks()->get()->makeHidden('pivot');

        return response($user);
    }
}
