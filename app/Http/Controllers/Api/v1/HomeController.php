<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = auth('api')->user();
    }

    /**
     * @SWG\Get(
     *     path="/v1/test",
     *     summary="Test auth route",
     *     tags={"Auth"},
     *     description="Test auth route",
     *     operationId="Test auth",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operationss",
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthenticated",
     *     ),
     *     security={
     *       {"Bearer": {}}
     *     },
     *     deprecated=true
     * )
     */
    public function index()
    {
        return $this->user;
    }
}
