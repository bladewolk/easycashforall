<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::when(request('role'), function ($query) {
            $query->whereHas('roles', function ($role) {
                $role->whereName(request('role'));
            });
        })->paginate(20);

        return view('dashboard.user.index', [
            'users' => $users
        ]);
    }
}
