<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function index()
    {
        $items = Transaction::when(request('status'), function ($query) {
            if (request('status') != 'all')
                $query->where('status', request('status'));
            if (request('payment') != 'all') {
                $query->where('payment', request('payment'));
            }
        })
            ->paginate(20);

        return view('dashboard.transactions.index', [
            'items' => $items
        ]);
    }
}
