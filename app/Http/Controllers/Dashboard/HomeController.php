<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\QiwiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $qiwi;

    public function __construct(QiwiHelper $qiwiHelper)
    {
        $this->qiwi = $qiwiHelper;
    }


    public function index()
    {
        return view('dashboard.home', [
            'qiwi' => $this->qiwi->getBalance(),
        ]);
    }
}
