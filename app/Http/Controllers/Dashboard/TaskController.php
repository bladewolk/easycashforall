<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Task;
use App\models\TaskCondition;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Raulr\GooglePlayScraper\Scraper;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $tasks = Task::latest()->with('conditions')
            ->when($request->has('status'), function ($query) use ($request) {
                if ($request->input('status') == 'active')
                    $query->where('expiry_date', '>', Carbon::now());
                if ($request->input('status') == 'done')
                    $query->where('expiry_date', '<', Carbon::now());
            })
            ->paginate(20);

        return view('dashboard.task.index', [
            'tasks' => $tasks
        ]);
    }

    public function create()
    {
        return view('dashboard.task.create', [
            'task' => new Task()
        ]);
    }

    public function store(Request $request)
    {
        try {
            $scrapper = new Scraper();
            $res = $scrapper->getApp($request->input('package'));
        } catch (Exception $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['package' => 'Invalid package name']);
        }

        $this->validate($request, [
            'package' => 'required',
            'reward' => 'required|numeric',
            'stars' => 'required',
            'expiry_date' => 'required|after:today',
            'text' => 'required|array',
            'type' => 'required|array',
        ]);

        $task = new Task();
        $task->fill($request->only(
            'package',
            'expiry_date',
            'stars',
            'reward'
        ));
        $task->fill($res);
        $task->save();

        $conditions = [];
        foreach ($request->input('type') as $index => $item) {
            $conditions[$index]['type'] = $request->type[$index];
            $conditions[$index]['text'] = $request->text[$index];
            $conditions[$index]['repeating_count'] = $request->repeating_count[$index];
        }

        $task->conditions()->createMany($conditions);

        return redirect()->route('tasks.index');
    }

    public function edit(Task $task)
    {
        return view('dashboard.task.edit', [
            'task' => $task
        ]);
    }

    public function destroy(Task $task)
    {
        $task->conditions()->delete();
        $task->delete();

        return redirect()
            ->back()
            ->with([
                'success' => 'The task was deleted!'
            ]);
    }
}
