<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
    CONST IN_PROGRESS = 'IN_PROGRESS';
    CONST COMPLETED = 'COMPLETED';
    CONST REPEATER_WAITING = 'REPEATER_WAITING';

    protected $fillable = [
        'user_id', 'task_id', 'status'
    ];

}
