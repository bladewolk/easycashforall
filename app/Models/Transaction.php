<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    CONST SUCCESS = 'SUCCESS';
    CONST ERROR = 'ERROR';
    CONST QIWI = 'QIWI';
    CONST YANDEX_MONEY = 'YANDEX_MONEY';
    CONST WEBMONEY = 'WEBMONEY';
    CONST PAYPAL = 'PAYPAL';

    protected $fillable = [
        'response_code',
        'error_code',
        'error_description',
        'status',
        'payment',
        'amount',
        'transaction_description'
    ];

    protected $guarded = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
