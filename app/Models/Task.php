<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Task extends Model
{
    CONST CANCELED = "CANCELED";
    CONST NEW = "NEW";

    protected $fillable = [
        'package',
        'image',
        'title',
        'expiry_date',
        'stars',
        'reward'
    ];

    protected $dates = [
        'expiry_date',
    ];

    protected $appends = [
        'status',
    ];

    protected $hidden = [
        'expiry_date',
        'created_at',
        'updated_at'
    ];


//    Add current task status
    public function getStatusAttribute()
    {
        $acceptedTask = auth()->user()->tasks()->whereTaskId($this->id)->first();
        if ($acceptedTask)
            $status = $acceptedTask->pivot->status;
        if ($this->expiry_date < Carbon::now())
            $status = self::CANCELED;

        return isset($status) ? $status : self::NEW;
    }

    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = Carbon::parse($value);
    }

    //Detailed info about Task
    public function conditions()
    {
        return $this->hasMany(TaskCondition::class);
    }


    public function user()
    {
        return $this->belongsToMany(User::class, 'user_tasks')
            ->where('user_id', auth()->user()->id)
            ->withPivot('status')
            ->withTimestamps()
            ->first();
    }
}
