<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class TaskCondition extends Model
{
//    Types
    CONST ONE_TIME = 'ONE_TIME';
    CONST ONE_TIME_INSTALL = 'ONE_TIME_INSTALL';
//    CONST COMPLETE = 'COMPLETE';
    CONST REPEATER = 'REPEATER';

    protected $fillable = [
        'task_id',
        'text',
        'type',
        'repeating_count',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'id',
        'task_id',
        'repeating_count',
        'status',
        'type'
    ];

    protected $appends = [
        'status'
    ];

    public function getStatusAttribute()
    {
        $userAcceptThisTask = auth()
            ->user()
            ->taskConditions()
            ->where('task_condition_id', $this->id)
            ->first();

        return $userAcceptThisTask
            ? $userAcceptThisTask->pivot->status
            : null;
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_task_conditions')
            ->where('user_id', auth()->user()->id)
            ->withPivot('status', 'repeated')
            ->withTimestamps()
            ->first();
    }
}
