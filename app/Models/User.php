<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    CONST ADMIN = 'admin';
    CONST USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'invite_code', 'promocode', 'balance'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'invite_code'
    ];

    protected $guarded = [
        'balance', 'promocode'
    ];

    protected $appends = [
        'completed_tasks_count',
        'referal_income'
    ];

//After complete task update balance
    public function updateBalance($value)
    {
        $referer = $this->referer();
//        Update referer balance
        if ($referer) {
            $percent = ($value * 0.2);
            $referer->pivot->update([
                'profit' => $referer->pivot->profit + $percent
            ]);
            $referer->update([
                'balance' => $referer->balance + $percent
            ]);
        }

        $this->update([
            'balance' => $this->balance + $value
        ]);
    }

    public function roles()
    {
        return $this->belongsToMany(Roles::class, 'role_user', 'user_id', 'role_id');
    }

    public function referals()
    {
        return $this->belongsToMany(User::class, 'referals', 'user_id', 'referal_id')
            ->withTimestamps()
            ->withPivot('profit');
    }

    public function referer()
    {
        return $this->belongsToMany(User::class, 'referals', 'referal_id', 'user_id')
            ->withTimestamps()
            ->withPivot('profit')
            ->first();
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function hasRole($role)
    {
        return null != $this->roles()->whereName($role)->first();
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'user_tasks')
            ->withPivot('status')
            ->withTimestamps();
    }

    public function taskConditions()
    {
        return $this->belongsToMany(TaskCondition::class, 'user_task_conditions')
            ->withPivot('status', 'repeated')
            ->withTimestamps();
    }

    public function completedTasks()
    {
        return $this->belongsToMany(Task::class, 'user_tasks')
            ->withPivot('status')
            ->withTimestamps()
            ->where('status', UserTask::COMPLETED);
    }

//Completed task count
    public function getCompletedTasksCountAttribute()
    {
        return $this->completedTasks()->count();
    }

    public function getReferalIncomeAttribute()
    {
        return $this->referals()->sum('profit');
    }

}
