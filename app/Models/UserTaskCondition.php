<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTaskCondition extends Model
{
    CONST DONE = 'DONE';
//    CONST WAITING = 'WAITING';
    CONST AVAILABLE = 'AVAILABLE';
    CONST NOT_AVAILABLE = 'NOT_AVAILABLE';

    protected $fillable = [
        'user_id',
        'task_condition_id',
        'status',
        'repeated'
    ];

}
