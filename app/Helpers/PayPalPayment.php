<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 10:55 AM
 */

namespace App\Helpers;


class PayPalPayment implements PaymentInterface
{
    protected $_url;
    protected $_token;

    public function __construct()
    {
        $this->_token = env('PAYPAL_TOKEN');
        $this->_url = 'https://api.sandbox.paypal.com/';
    }

    public function sendRequest(string $method, array $content = [], bool $post = false)
    {
        $ch = curl_init();
        if ($post) {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method . '/?' . http_build_query($content));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
            'Authorization: Bearer ' . $this->_token
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return array_merge(json_decode($result, true), ['httpcode' => $httpcode]);
    }

    public function getAccount(array $params = [])
    {
        //
    }

    public function getBalance()
    {
        return $this->sendRequest('v2/wallet/balance-accounts', [], false);
    }

    /**
     * @param string $wallet -> PayPal email
     * @param float $amount
     * @return array
     */
    public function transferMoney(string $wallet, float $amount)
    {
//        TODO activate card and test
//        Only send by email
        $item = (object)[
            "recipient_type" => "EMAIL",
            "amount" => [
                "value" => $amount,
                "currency" => "USD"
            ],
            'note' => "Thanks for your patronage!",
            'sender_item_id' => "201403140001",
            "receiver" => $wallet
        ];

        $content = [
            "sender_batch_header" => [
                'sender_batch_id' => "2014021801",
                "email_subject" => "You have a payout!",
                "email_message" => "You have received a payout! Thanks for using our service!"
            ],
            "items" => [
                $item
            ]
        ];


        return $this->sendRequest('v1/payments/payouts', $content, true);
    }

    public function logTransaction(array $result, string $wallet, float $amount)
    {
        //
    }
}