<?php

namespace App\Helpers;


use App\Models\Task;
use App\models\TaskCondition;
use App\models\UserTask;
use App\Models\UserTaskCondition;
use Illuminate\Support\Carbon;

class TaskHelper
{
    public static function completeConditions($conditions)
    {
        $conditions->map(function ($item) {

//            Task available
            if ($item->status == UserTaskCondition::AVAILABLE) {

                if ($item->type == TaskCondition::ONE_TIME || $item->type == TaskCondition::ONE_TIME_INSTALL)
                    $item->pivot->update([
                        'status' => UserTaskCondition::DONE
                    ]);

                if ($item->type == TaskCondition::REPEATER) {

                    if ($item->repeating_count > $item->pivot->repeated)
                        $item->pivot->update([
                            'status' => (++$item->pivot->repeated == $item->repeating_count)
                                ? UserTaskCondition::DONE
                                : UserTaskCondition::NOT_AVAILABLE,
//                          ++null => 1
                            'repeated' => $item->pivot->repeated
                        ]);
                }
            }
        });

        return $conditions;
    }

    public static function updateGlobalTaskStatus($task, $conditions)
    {
        $status = $conditions->groupBy('status')->keys();
        if ($task->status != UserTask::COMPLETED && $status->count() == 1 && $status->first() == UserTaskCondition::DONE) {
            $task->pivot->update([
                'status' => UserTask::COMPLETED
            ]);
            auth('api')->user()->updateBalance($task->reward);
        }

        return $task;
    }

    public static function getAndModifyTasks($tasks)
    {
        $res = [];

        foreach ($tasks as $index => $task) {
            $task->available = true;

            if ($task->status == UserTask::COMPLETED) {
//                Remove if task completed
                unset($tasks[$index]);
                continue;
            }

            if ($task->status == Task::CANCELED)
                $task->available = false;

            if ($task->status == UserTask::IN_PROGRESS) {

                foreach ($task->conditions as $condition) {

                    if ($condition->type == TaskCondition::REPEATER) {
                        $userTaskCondition = $condition->user();

                        if ($userTaskCondition->pivot->status == UserTaskCondition::NOT_AVAILABLE) {
                            $diff = Carbon::now()->diffInMinutes($userTaskCondition->pivot->updated_at);
                            //1440 minutes in 24 hours
                            if ($diff > 1440) {
                                $userTaskCondition->pivot->update([
                                    'status' => UserTaskCondition::AVAILABLE
                                ]);
                            } else
                                $task->available = false;
                        }

                    }
                }

            }

            $task->timeRemaining = self::convertTimeRemaining($task->expiry_date);

            $temp = $task->toArray();
            $temp['conditions'] = $task->conditions->pluck('text')->all();
            $res[] = $temp;
        }

        return $res;
    }

    public static function convertTimeRemaining($end)
    {
        $result = null;
        $days = Carbon::now()->diffInDays($end);
        $hours = Carbon::now()->addDays($days)->diffInHours($end);
        $minutes = Carbon::now()->addDays($days)->addHours($hours)->diffInMinutes($end);

        if ($days)
            return $days . 'д ' . $hours . 'ч ' . $minutes . 'м';
        elseif ($hours)
            return $hours . 'ч ' . $minutes . 'м';
        else
            return $minutes . 'м';
    }

}