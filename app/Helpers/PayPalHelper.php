<?php

namespace App\Helpers;


use GuzzleHttp\Client;

class PayPalHelper
{
    protected $client;
    private $token;


    public function __construct(Client $client)
    {
        $this->token = env('PAYPAL_TOKEN');
        $this->client = $client;
        $this->headers = [
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
            'Authorization' => 'Bearer ' . env('PAYPAL_TOKEN'),
        ];
    }

    public function checkToken()
    {
        $response = $this->client->request('GET', 'https://api.sandbox.paypal.com/v1/oauth2/token/userinfo?schema=openid', [
            'headers' => $this->headers
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function getBalance()
    {
//        Privilegies
        return $this->client->request('GET', 'https://api.sandbox.paypal.com/v2/wallet/balance-accounts', [
            'headers' => $this->headers
        ]);
    }

}