<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class QiwiHelper
{
    private $_phone;
    private $_token;
    private $_url;

    function __construct()
    {
        $this->_phone = time();
        $this->_token = env('QIWI_TOKEN');
        $this->_url = 'https://edge.qiwi.com/';
    }

    private function sendRequest($method, $content = [], $post = false)
    {
        $ch = curl_init();
        if ($post) {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method . '/?' . http_build_query($content));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->_token
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    public function getAccount($params = [])
    {
        return $this->sendRequest('person-profile/v1/profile/current', $params);
    }

    public function getPaymentsHistory(Array $params = [])
    {
        return $this->sendRequest('payment-history/v1/persons/' . $this->_phone . '/payments', $params);
    }

    public function getPaymentsStats(Array $params = [])
    {
        return $this->sendRequest('payment-history/v1/persons/' . $this->_phone . '/payments/total', $params);
    }

    public function getBalance()
    {
        return $this->sendRequest('funding-sources/v1/accounts/current')['accounts'][0];
    }

    public function getTax($providerId)
    {
        return $this->sendRequest('sinap/providers/' . $providerId . '/form');
    }

    public function sendMoneyToQiwi(string $wallet, float $amout)
    {
        $params = [
            "id" => (string)(time() * 1000),
            "sum" => [
                "amount" => $amout,
                "currency" => "643"
            ],
            "paymentMethod" => [
                "type" => "Account",
                "accountId" => "643"
            ],
            "comment" => "test",
            "fields" => [
                "account" => $wallet,
            ]
        ];
        return $this->sendRequest('sinap/api/v2/terms/99/payments', $params, 1);
    }

    public function sendMoneyToProvider($providerId, Array $params = [])
    {
        return $this->sendRequest('sinap/terms/' . $providerId . '/payments', $params, 1);
    }
}