<?php

namespace App\Helpers;

use App\Models\Transaction;

class QiwiPayment implements PaymentInterface
{
    private $_phone;
    private $_token;
    private $_url;

    function __construct()
    {
        $this->_phone = time();
        $this->_token = env('QIWI_TOKEN');
        $this->_url = 'https://edge.qiwi.com/';
    }

    function sendRequest(string $method, array $content = [], bool $post = false)
    {
        $ch = curl_init();
        if ($post) {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method . '/?' . http_build_query($content));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->_token
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return array_merge(json_decode($result, true), ['httpcode' => $httpcode]);
    }

    function getAccount(array $params = [])
    {
        return $this->sendRequest('person-profile/v1/profile/current', $params);
    }

    function getBalance()
    {
        return $this->sendRequest('funding-sources/v1/accounts/current')['accounts'][0]['balance']['amount'];
    }

    function transferMoney(string $wallet, float $amount)
    {
        $params = [
            "id" => (string)(time() * 1000),
            "sum" => [
                "amount" => $amount,
                "currency" => "643"
            ],
            "paymentMethod" => [
                "type" => "Account",
                "accountId" => "643"
            ],
            "comment" => "test",
            "fields" => [
                "account" => $wallet,
            ]
        ];
        $result = $this->sendRequest('sinap/api/v2/terms/99/payments', $params, 1);
        $this->logTransaction($result, $wallet, $amount);

        return $result['httpcode'] == 200 ? true : false;
    }

    public function logTransaction(array $result, string $wallet, float $amount)
    {
//        Todo change auth to api auth
        $user = auth()->user();

        if ($result['httpcode'] != 200) {
            $user->transactions()->create([
                'payment' => Transaction::QIWI,
                'transaction_description' => 'Вывод средств на Qiwi кошелек ' . $wallet,
                'amount' => $amount,
                'response_code' => $result['httpcode'],
                'error_code' => $result['code'],
                'error_description' => $result['message'],
                'status' => Transaction::ERROR
            ]);
        } else {
            $user->transactions()->create([
                'payment' => Transaction::QIWI,
                'transaction_description' => 'Вывод средств на Qiwi кошелек ' . $wallet,
                'amount' => $amount,
                'response_code' => $result['httpcode'],
                'status' => Transaction::SUCCESS
            ]);
        }
    }
}