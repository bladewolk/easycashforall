<?php

namespace App\Helpers;


class YandexPayment implements PaymentInterface
{
    protected $_token;
    protected $_url;

    public function __construct()
    {
        $this->_url = 'https://money.yandex.ru/api/';
        $this->_token = env('YANDEX_ACCESS_TOKEN');
    }

    function sendRequest(string $method, array $content = [], bool $post = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_url . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($content));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $this->_token
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    function getBalance()
    {
        return $this->sendRequest('account-info', [], true)['balance'];
    }

    function getAccount(array $params = [])
    {
        return $this->sendRequest('account-info', []);
    }

    function transferMoney(string $wallet, float $amout)
    {
        $body = [
            'pattern_id' => 'p2p',
            'to' => '410016087338822',
            // 'amount' => $amout,
            'amount_due' => 0.01,
            'comment' => 'comment',
            'message' => 'message',
            'label' => 'label',
            'test_payment' => "true",
            'test_result' => 'success'
        ];


        $requestPayment = $this->sendRequest('request-payment', $body, true);

        if ($requestPayment['status'] == 'success') {
//            Confirm payment
            $data = [
                'request_id' => $requestPayment['request_id'],
                'money_source' => 'wallet',
                'test_payment' => "true",
                'test_result' => 'success'
            ];

            $processPayment = $this->sendRequest('process-payment', $data);

            if ($processPayment['status'] == 'success')
//                TODO write logger
                return true;
        }

        return false;
    }


    public function logTransaction(array $result, string $wallet, float $amount)
    {
        // TODO: Implement logTransaction() method.
    }
}
