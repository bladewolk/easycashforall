<?php

namespace App\Helpers;


use baibaratsky\WebMoney\Api\X\X2\Request;
use baibaratsky\WebMoney\Request\Requester\CurlRequester;
use baibaratsky\WebMoney\Signer;
use baibaratsky\WebMoney\WebMoney;

class WebMoneyHelper
{

    public function sendMoney(string $payeePurse)
    {
//        TODO need personal certificate.
        $webMoney = new WebMoney(new CurlRequester);

        $request = new Request;
        $request->setSignerWmid(env('WEB_MONEY_WMID'));
        $request->setTransactionExternalId(1); // Unique ID of the transaction in your system
        $request->setPayerPurse(env('WEB_MONEY_PURSE'));
        $request->setPayeePurse($payeePurse);
        $request->setAmount(0.01); // Payment amount
        $request->setDescription('Test payment');

        $request->sign(new Signer(env('WEB_MONEY_WMID'), public_path('232499591610.kwm'), '42434243'));

        if ($request->validate()) {
            /** @var X2\Response $response */
            $response = $webMoney->request($request);
            if ($response->getReturnCode() === 0) {
                echo 'Successful payment, transaction id: ' . $response->getTransactionId();
            } else {
                echo 'Payment error: ' . $response->getReturnDescription();
            }
        } else {
            echo 'Request errors: ' . PHP_EOL;
            foreach ($request->getErrors() as $error) {
                echo ' - ' . $error . PHP_EOL;
            }
        }
    }
}