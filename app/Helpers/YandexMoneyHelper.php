<?php

namespace App\Helpers;

class YandexMoneyHelper
{
    private $_clientID;
    private $_token;
    private $_url;

    public function __construct()
    {
        $this->_token = env('YANDEX_ACCESS_TOKEN');
        $this->_clientID = env('YANDEX_MONEY_APP_ID');
        $this->_url = 'https://money.yandex.ru';
    }

    private function sendRequest($method, $content = [], $post = false)
    {
        $ch = curl_init();
        if ($post) {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($content));
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->_url . $method . '?' . http_build_query($content));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $this->_token
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        dd($result);

        return json_decode($result, true);
    }

    public function accountInfo()
    {
        return $this->payment('41001101140', 0.01);
        return $this->sendRequest('/api/account-info', [], true);
    }

    public function payment($wallet, $amount)
    {
        $data = [
            'pattern_id' => 'p2p',
            'to' => $wallet,
            // 'amount' => $amount,
            'amount_due' => $amount,
            'comment' => 'TEST',
            'message' => 'Transfer money from easyCash',
            // Test payment
            // 'test_payment' => true,
            // 'test_card' => $wallet,
            // 'test_result' => 'success'
        ];

        return $this->sendRequest('/api/request-payment', $data, true);
    }
}