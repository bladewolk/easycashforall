<?php

namespace App\Helpers;


interface PaymentInterface
{
    public function sendRequest(string $method, array $content = [], bool $post = false);

    public function getAccount(array $params = []);

    public function getBalance();

    public function transferMoney(string $wallet, float $amount);

    public function logTransaction(array $result, string $wallet, float $amount);
}