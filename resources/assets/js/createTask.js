$(document).ready(function () {

    $('#addCondition').click(function () {
        let first = $('.conditionRow:first').clone();
        first.closest('.conditionRow').find('input[type=text]').each(function (index, item) {
            item.value = '';
        });
        first.insertAfter('.conditionRow:last');
        $('select').material_select();
    });

    $(document).on("click", ".removeCondition", function () {
        if ($(".conditionRow").length > 1)
            $(this).closest(".conditionRow").remove();
    });

    $('.task-type-select').each(function () {
        if ($(this).val() == 'REPEATER')
            $(this).closest(".conditionRow").find('.repeater-field').css("display", "block");
    });


    $('#createTaskForm').submit(function () {
        $('input').each(function (index, item) {
            if (item.value == '' && item.id != 'repeating_count') {
                $(this).addClass('invalid');
                event.preventDefault();
                Materialize.toast('Please fill all fields!', 1000) // 4000 is the duration of the toast
            }
        });

        return true;
    });

    $(document).on("change", '.task-type-select', function () {
        if ($(this).is('select')) {
            if ($(this).val() === 'REPEATER')
                $(this).closest(".conditionRow").find('.repeater-field').css("display", "block");
            else
                $(this).closest(".conditionRow").find('.repeater-field').css("display", "none");
        }
    });
});

