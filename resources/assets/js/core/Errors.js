export default class Errors {
    constructor(errors) {
        this.errors = errors;
    }

    has(field) {
        return true
    }

    any() {
        return false
    }

    reset() {
        this.errors = null
    }
}