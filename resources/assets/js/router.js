import Vue from 'vue';
import VueRouter from 'vue-router';

import LoginComponent from './components/LoginComponent'
import RegisterComponent from './components/RegisterComponent'
import LayoutComponent from './components/LayoutComponent'
import HomeComponent from './components/HomeComponent'
import TaskComponent from './components/tasks/TaskComponent'
import CreateTask from './components/tasks/CreateTask'
import UsersComponent from './components/UsersComponent'

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        component: LoginComponent
    },
    {
        path: '/logout',
        redirect: '/login'
    },
    {
        path: '/register',
        component: RegisterComponent
    },
    {
        path: '/',
        redirect: 'dashboard',
        meta: {requiredAuth: true},
        component: LayoutComponent,
        children: [
            {
                path: 'dashboard',
                name: 'Home',
                meta: {requiredAuth: true},
                component: HomeComponent
            },
            {
                path: 'tasks',
                name: 'Tasks',
                meta: {requiredAuth: true},
                component: TaskComponent,
            },
            {
                path: 'tasks/create',
                name: 'Create Task',
                meta: {requiredAuth: true},
                component: CreateTask
            },
            {
                path: 'users',
                name: 'Users',
                meta: {requiredAuth: true},
                component: UsersComponent
            }
        ]
    }
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    let token = window.localStorage.getItem('access_token');

    if (to.meta.requiredAuth === true) {
        if (token !== null)
            next()
        else
            next('login')
    }
    else {
        if (to.path === '/login' && token || to.path === '/register' && token)
            next('dashboard');
        else
            next()
    }
});

export default router;