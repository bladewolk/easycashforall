window.axios = require('axios');
import _ from 'lodash'
import Vue from 'vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import router from './router'
import store from './store'

Vue.use(VueMaterial);

const app = new Vue({
    router,
    store,
    el: '#app'
});
