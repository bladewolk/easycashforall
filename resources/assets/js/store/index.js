import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth'
import tasks from './modules/task'
import users from './modules/users'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        fetch: false,
        menuVisible: window.localStorage.getItem('menuVisible') === 'false' ? false : true
    },

    mutations: {
        toggleMenuVisible(state) {
            window.localStorage.setItem('menuVisible', !state.menuVisible);
            state.menuVisible = !state.menuVisible
        },

        toggleGlobalFetch(state) {
            state.fetch = !state.fetch
        }
    },

    modules: {
        auth,
        tasks,
        users
    }
})