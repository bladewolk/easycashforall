import authAPI from '../../api/auth'

const state = {
    token: window.localStorage.getItem('access_token'),
    fetch: false,
    errors: {
        email: null,
        password: null,
        another: null
    }
};

const mutations = {
    recordErrors(state, errors) {
        Object.keys(errors).map(function (key) {
            state.errors[key] = errors[key][0]
        })
    },

    clearError(state, errorName) {
        state.errors[errorName] = null
    },

    toggleFetch(state) {
        state.fetch = !state.fetch
    },

    saveToken(state, token) {
        window.localStorage.setItem('access_token', token);
        state.access_token = token
        location.reload()
    },

    removeToken(state) {
        window.localStorage.clear();
        state.access_token = null;
        location.reload()
    }
};

const actions = {
    tryLogin({commit, rootState}, payload) {
        commit('toggleFetch');

        authAPI.login(payload)
            .then(resolve => {
                    commit('toggleFetch');
                    commit('saveToken', resolve.data.access_token);
                },
                reject => {
                    commit('toggleFetch');

                    if (reject.status === 422)
                        commit('recordErrors', reject.data.errors)
                });
    },

    tryRegister({commit, state}, payload) {
        commit('toggleFetch');

        authAPI.register(payload)
            .then(resolve => {
                    commit('toggleFetch');
                    commit('saveToken', resolve.data.access_token)
                },
                reject => {
                    commit('toggleFetch');

                    if (reject.status === 422)
                        commit('recordErrors', reject.data.errors)
                });

    }
};

export default {
    state,
    actions,
    mutations
}