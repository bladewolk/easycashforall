import taskAPI from '../../api/task'

const state = {
    tasks: [],
    create: {
        form: {
            package: null,
            stars: 1,
            reward: null,
            expiry_date: new Date(),
            conditions: [{
                type: 'ONE_TIME',
                text: null,
                repeating_count: null,
            }]
        },

        errors: {},

        stepper: {
            first: {
                done: false,
                error: null
            },
            second: {
                done: false,
                error: null
            },
            third: {
                done: false,
                error: null
            }
        },
        activeStep: 'first',
        fetch: false,
    }
};

const mutations = {
    toggleFetch(state) {
        state.create.fetch = !state.create.fetch
    },

    writeStepError(state, payload) {
        state.create.stepper[payload.step].error = payload.error
    },

    writeValidationErrors(state, errors) {
        for (let field in errors) {
            state.create.errors[field] = errors[field][0]
        }
    },

    completeStep(state, payload) {
        state.create.stepper[payload.step].error = null;
        state.create.stepper[payload.step].done = true;
        state.create.activeStep = payload.next
    },

    addCondition(state) {
        state.create.form.conditions.push({
            type: 'ONE_TIME',
            text: null,
            repeating_count: null,
        })
    },

    removeCondition(state, id) {
        state.create.form.conditions = state.create.form.conditions.filter((item, key) => {
            return key !== id
        })
    },

    loadTasks(state, payload) {
        state.tasks = payload
    }
};

const actions = {
    completeStep({state, dispatch}, step) {
        if (step === 'first')
            dispatch('completeFirstStep', state.create.form);

        if (step === 'second')
            dispatch('completeSecondStep');
    },

    createTask({state, dispatch, commit}) {
        commit('toggleFetch');

        return new Promise((success, reject) => {
            taskAPI.storeTask(state.create.form)
                .then(resolve => {
                        commit('toggleFetch');
                        success()
                    },
                    reject => {
                        commit('toggleFetch');

                        if (reject.status === 422) {
                            commit('writeStepError', {
                                step: 'third',
                                error: 'Validation error'
                            });
                            commit('writeValidationErrors', reject.data.errors);
                        }
                    })
        })

    },

    completeFirstStep({state, commit}, form) {
        commit('toggleFetch');

        taskAPI.checkPackage(form)
            .then(resolve => {
                    commit('toggleFetch');
                    commit('completeStep', {
                        step: 'first',
                        next: 'second'
                    })
                },
                reject => {
                    commit('toggleFetch');

                    if (reject.status === 422) {
                        commit('writeValidationErrors', reject.data.errors);
                        commit('writeStepError', {
                            step: 'first',
                            error: reject.data.errors.package[0]
                        })
                    }

                })
    },

    completeSecondStep({state, commit}) {
        commit('toggleFetch');

        taskAPI.checkTaskDescription(state.create.form)
            .then(resolve => {
                    commit('toggleFetch');
                    commit('completeStep', {
                        step: 'second',
                        next: 'third'
                    });
                },
                reject => {
                    commit('toggleFetch');

                    if (reject.status === 422) {
                        commit('writeValidationErrors', reject.data.errors);
                        commit('writeStepError', {
                            step: 'second',
                            error: 'Invalid data'
                        })
                    }

                })
    },

    getTasks({state, commit}) {
        commit('toggleFetch');

        taskAPI.getTasks()
            .then(resolve => {
                commit('toggleFetch');
                commit('loadTasks', resolve.data);
                console.log('resolve', resolve)
            }, reject => {
                commit('toggleFetch');

                console.log('reject', reject)
            })
    }

};


export default {
    state,
    mutations,
    actions
}