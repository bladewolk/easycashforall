import userAPI from '../../api/users'

const state = {
    paginator: {},
    users: [],
    selected: null,
    search: '',
    role: ''
};

const mutations = {

    updateSearch(state, newValue) {
        state.search = newValue;
    },

    changeRole(state, role) {
        state.role = role
    },

    loadUsers(state, payload) {
        state.users = payload
    },

    loadPaginator(state, {current_page, last_page, per_page, total}) {
        state.paginator = {
            current_page,
            last_page,
            per_page,
            total
        }
    }

};

const actions = {

    getUsers({state, commit, rootState}, router) {
        let requestData = {
            search: state.search,
            role: state.role,
            page: router.page
        };

        commit('toggleGlobalFetch');

        userAPI.get(requestData)
            .then(res => {
                    commit('toggleGlobalFetch');
                    commit('loadUsers', res.data.data);
                    commit('loadPaginator', res.data);
                },
                reject => {
                    commit('toggleGlobalFetch');
                });
    },

    deleteUsers({state, commit, dispatch}, ids) {
        commit('toggleGlobalFetch');


        userAPI.delete({data: {ids}})
            .then(resolve => {
                commit('toggleGlobalFetch');
                dispatch('getUsers', {page: state.paginator.current_page})
            }, reject => {
                commit('toggleGlobalFetch');
            })
    }
};

export default {
    state,
    mutations,
    actions
}