export default {
    checkPackage(payload) {
        return new Promise((resolve, reject) => {
            axios.post('/api/v2/tasks/checkPackage', payload)
                .then(response => {
                    resolve(response);
                })
                .catch(e => {
                    reject(e.response);
                })
        })
    },

    checkTaskDescription(payload) {
        return new Promise((resolve, reject) => {
                axios.post('/api/v2/tasks/checkDescription', payload)
                    .then(res => resolve(res))
                    .catch(e => reject(e.response))
            }
        )
    },

    storeTask(payload) {
        return new Promise((resolve, reject) => {
            axios.post('/api/v2/tasks/store', payload)
                .then(res => resolve(res))
                .catch(e => reject(e.response))
        })
    },

    getTasks() {
        return new Promise((resolve, reject) => {
            axios.get('/api/v2/tasks')
                .then(res => resolve(res))
                .catch(e => reject(e))
        })
    }
}