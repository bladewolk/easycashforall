export default {
    get(data) {
        return new Promise((resolve, reject) => {
            axios.get('/api/v2/users', {
                params: data
            })
                .then(res => resolve(res))
                .catch(e => reject(e))
        })
    },

    delete(request) {
        return new Promise((resolve, reject) => {
            axios.delete('/api/v2/users', request)
                .then(res => resolve(res))
                .catch(e => reject(e))
        })
    }
}