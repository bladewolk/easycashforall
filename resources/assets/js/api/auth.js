export default {
    login(form) {
        return new Promise((resolve, reject) => {
            axios.post('/api/v2/login', form)
                .then(res => resolve(res))
                .catch(e => reject(e.response))
        })
    },

    register(form) {
        return new Promise((resolve, reject) => {
            axios.post('/api/v2/register', form)
                .then(res => resolve(res))
                .catch(e => reject(e.response))
        })
    }
}