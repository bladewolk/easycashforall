@extends('layouts.app')
@section('content')
    <div class="content">
        <form action="">
            <div class="row">
                <div class="input-field col s2 m2">
                    <select name="status" onchange="this.form.submit()">
                        <option value="all" {{ request('status') == 'all' ? 'selected' : null }}>All</option>
                        <option value="success" {{ request('status') == 'success' ? 'selected' : null }}>Success
                        </option>
                        <option value="error" {{ request('status') == 'error' ? 'selected' : null }}>Error</option>
                    </select>
                    <label>Status</label>
                </div>
                <div class="input-field col s2 m2">
                    <select name="payment" onchange="this.form.submit()">
                        <option value="all" {{ request('payment') == 'all' ? 'selected' : null }}>All</option>
                        <option value="QIWI" {{ request('payment') == 'QIWI' ? 'selected' : null }}>QIWI</option>
                        <option value="YANDEX_MONEY" {{ request('payment') == 'YANDEX_MONEY' ? 'selected' : null }}
                        >YandexMoney
                        </option>
                        <option value="WEBMONEY" {{ request('payment') == 'WEBMONEY' ? 'selected' : null }}>WebMoney
                        </option>
                        <option value="PAYPAL" {{ request('payment') == 'PAYPAL' ? 'selected' : null }}>PayPal</option>
                    </select>
                    <label>Payment</label>
                </div>
            </div>
        </form>

        @if($items->total())
            <p>Total: {{ $items->total() }}</p>
            <ul class="collapsible popout" data-collapsible="accordion">
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($items as $item)
                        <li>
                            <div class="collapsible-header">
                                {{ $item->status }}
                            </div>
                            <div class="collapsible-body">
                                @if($item->status == 'SUCCESS')
                                    <p>User: {{ $item->user->email }}</p>
                                    <p>Payment: {{ $item->payment }}</p>
                                    <p>Amount: {{ $item->amount }}</p>
                                    <p>Transaction description: {{ $item->transaction_description }}</p>
                                    <p>Date: {{ $item->created_at }}</p>
                                @endif
                                @if($item->status == 'ERROR')
                                    <p>User: {{ $item->user->email }}</p>
                                    <p>Response code: {{ $item->response_code }}</p>
                                    <p>Error code: {{ $item->error_code }}</p>
                                    <p>Error description: {{ $item->error_description }}</p>
                                    <p>Payment: {{ $item->payment }}</p>
                                    <p>Amount: {{ $item->amount }}</p>
                                    <p>Transaction description: {{ $item->transaction_description }}</p>
                                    <p>Date: {{ $item->created_at->format('d/m/y') }}</p>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ul>
            </ul>
        @endif
        {{ $items->links() }}
    </div>
@endsection

