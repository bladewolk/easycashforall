@extends('layouts.app')
@section('content')
    <div class="content">
        <form action="">
            <div class="input-field col s2 m2">
                <select name="role" onchange="this.form.submit()">
                    <option value="admin" {{ request('role') == 'admin' ? 'selected' : null }}>Admin</option>
                    <option value="user" {{ request('role') == 'user' ? 'selected' : null }}>User</option>
                </select>
                <label>Role</label>
            </div>
        </form>
        @if($users->total())
            <ul class="collapsible popout" data-collapsible="accordion">
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($users as $user)
                        <li>
                            <div class="collapsible-header">
                                {{ $user->email }}
                            </div>
                            <div class="collapsible-body">
                                <p>Name: {{ $user->name }}</p>
                                <p>Email: {{ $user->email }}</p>
                                <p>Register: {{ $user->created_at->format('d/m/y') }}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </ul>
        @endif
        {{ $users->links() }}
    </div>
@endsection