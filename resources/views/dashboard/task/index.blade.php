@extends('layouts.app')
@section('content')
    <div class="content">
        @if($tasks->total())
            <form action="">
                <div class="input-field col s2 m2">
                    <select name="status" onchange="this.form.submit()">
                        <option {{ request('status') === 'all' ? 'selected' : null }} value="all">All</option>
                        <option {{ request('status') === 'active' ? 'selected' : null }} value="active">Active</option>
                        <option {{ request('status') === 'done' ? 'selected' : null }} value="done">Done</option>
                    </select>
                    <label>Status</label>
                </div>
            </form>

            <ul class="collapsible" data-collapsible="accordion">
                @foreach($tasks as $task)
                    <li>
                        <div class="collapsible-header">
                            <img style="width: 20px; height: 20px" src="{{ $task->image }}" alt="image">
                            &nbsp
                            {{ $task->title }}
                            <span class="badge">{{ $task->expiry_date->format('d/m/Y h:i') }}</span>
                        </div>
                        <div class="collapsible-body">
                            <p class="flow-text">Conditions</p>
                            @foreach($task->conditions as $taskCondition)
                                <p>
                                    <span>{{ $loop->index + 1 }}. </span>{{ $taskCondition->text }}
                                    <span class="badge">{{ $taskCondition->type }}</span>
                                </p>
                            @endforeach
                            <p class="flow-text">Reward: {{ $task->reward }} руб</p>
                            <div class="row">
                                <form action="{{ route('tasks.destroy', ['id' => $task->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="waves-effect waves-light btn right pink lighten-1"
                                    >Delete
                                    </button>
                                </form>
                                {{--<a class="waves-effect waves-light btn right blue lighten-2"--}}
                                   {{--href="{{ route('tasks.edit', ['id' => $task->id]) }}">Edit</a>--}}
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        @else
            <p class="flow-text">Create you first task!</p>
        @endif

        {{ $tasks->links() }}

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href="{{ route('tasks.create') }}">
                <i class="material-icons">add</i>
            </a>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('select').material_select();
        });
    </script>
@endsection
