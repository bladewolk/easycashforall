@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form action="{{ route('tasks.store') }}" method="POST" id="createTaskForm">
                {{ csrf_field() }}
                @include('dashboard.task._form', ['button' => 'Create'])
            </form>
            <div class="divider"></div>
            <p class="flow-text">Types:</p>
            <div class="chip tooltipped" data-position="top" data-delay="50"
                 data-tooltip="Install one">
                One time install
            </div>
            <div class="chip tooltipped" data-position="top" data-delay="50"
                 data-tooltip="Random one time action">
                One time
            </div>
            <div class="chip tooltipped" data-position="top" data-delay="50"
                 data-tooltip="Repeater task 24 hours to repeat">
                Repeater
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @if ($errors->any())
        <script>
            @foreach ($errors->all() as $error)
            Materialize.toast('{{ $error }}', 4000);
            @endforeach
        </script>
    @endif
@endsection
