@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form action="{{ route('tasks.update', ['id' => $task->id]) }}" method="POST" id="createTaskForm">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                @include('dashboard.task._form', ['button' => 'Create'])
            </form>
        </div>
    </div>


@endsection
@section('scripts')
    @if ($errors->any())
        <script>
            @foreach ($errors->all() as $error)
            Materialize.toast('{{ $error }}', 4000);
            @endforeach
        </script>
    @endif
@endsection
