<p class="flow-text">Task</p>
<div class="row">
    <div class="input-field col s12 m4">
        <input placeholder="Package name" id="package" name="package" type="text"
               class="{{ $errors->has('package') ? 'invalid' : '' }}"
               autocomplete="off" required max="50" value="{{ old('package', $task->package) }}">
        <label for="package" data-error="{{ $errors->first('package') }}" data-success="right">Package
            name</label>
    </div>
    <div class="input-field col s12 m2">
        <select name="stars" class="validate">
            <option {{ old('stars', $task->stars) == '1' ? 'selected' : null }} value="1">1 star</option>
            <option {{ old('stars', $task->stars) == '2' ? 'selected' : null }} value="2">2 star</option>
            <option {{ old('stars', $task->stars) == '3' ? 'selected' : null }} value="3">3 star</option>
        </select>
        <label data-error="{{ $errors->first('stars') }}">Stars count</label>
    </div>
    <div class="input-field col s12 m2">
        <input placeholder="0.0" id="reward" name="reward" type="text"
               class="{{ $errors->has('reward') ? 'invalid' : '' }}"
               autocomplete="off" required max="50" value="{{ old('reward', $task->reward) }}">
        <label for="reward" data-error="{{ $errors->first('reward') }}" data-success="right">Reward</label>
    </div>
    <div class="input-field col s12 m4">
        <input id="expiry_date" name="expiry_date" type="text"
               @if($task->expiry_date)
               value="{{ old('expiry_date', $task->expiry_date->format('d F, Y')) }}"
               @else
               value="{{ old('expiry_date') }}"
               @endif
               class="datepicker validate {{ $errors->has('expiry_date') ? 'invalid' : '' }}" required>
        <label for="expiry_date" data-error="{{ $errors->first('expiry_date') }}">Expiry Date</label>
    </div>
</div>
<div class="divider"></div>
<p class="flow-text">Task conditions</p>
@if($errors->any())
    @foreach(old('type') as $type)
        <div class="row conditionRow">
            <div class="input-field col s2">
                <select name="type[]" class="task-type-select">
                    <option {{ old('type.'. $loop->index, null) == 'ONE_TIME' ? 'selected' : null }} value="ONE_TIME"
                    >One Time
                    </option>
                    <option {{ old('type.'. $loop->index, null) == 'ONE_TIME_INSTALL' ? 'selected' : null }}
                            value="ONE_TIME_INSTALL">One time install
                    </option>
                    <option {{ old('type.'. $loop->index, null) == 'REPEATER' ? 'selected' : null }}
                            value="REPEATER">Repeater
                    </option>
                </select>
                <label>Task type</label>
            </div>
            <div class="input-field col s3">
                <input name="text[]" placeholder="Text" type="text" class="validate" required
                       value="{{ old('text.'.$loop->index) }}" autocomplete="off">
                <label>Condition text</label>
            </div>
            <div class="input-field col s6 m1 repeater-field">
                <input placeholder="3" id="repeating_count" type="text" class="validate"
                       name="repeating_count[]" autocomplete="off"
                       value="{{ old('repeating_count.'.$loop->index) }}">
                <label for="repeating_count">Repeat count</label>
            </div>
            <a class="waves-effect waves-light btn right red lighten-1 removeCondition">Remove</a>
        </div>
    @endforeach
@else
    @forelse($task->conditions as $condition)
        <div class="row conditionRow">
            <div class="input-field col s2">
                <select name="type[]" class="task-type-select">
                    <option value="ONE_TIME">One time</option>
                    <option value="ONE_TIME_INSTALL">One time install</option>
                    <option value="REPEATER">Repeater</option>
                </select>
                <label>Task type</label>
            </div>
            <div class="input-field col s3">
                <input name="text[]" placeholder="Text" type="text" class="validate" required
                       value="{{ old('text', $condition->text) }}" autocomplete="off">
                <label>Condition text</label>
            </div>
            <div class="input-field col s6 m1 repeater-field">
                <input placeholder="3" id="repeating_count" type="text" class="validate"
                       name="repeating_count[]" autocomplete="off"
                       value="{{ old('repeating_count', $condition->repeating_count) }}">
                <label for="repeating_count">Repeat count</label>
            </div>
            <a class="waves-effect waves-light btn right red lighten-1 removeCondition">Remove</a>
        </div>
    @empty
        <div class="row conditionRow">
            <div class="input-field col s12 m3">
                <select name="type[]" class="task-type-select">
                    <option value="ONE_TIME">One time</option>
                    <option value="ONE_TIME_INSTALL">One time install</option>
                    <option value="REPEATER">Repeater</option>
                </select>
                <label>Task type</label>
            </div>
            <div class="input-field col s12 m3">
                <input name="text[]" placeholder="Text" type="text" class="validate" required
                       value="" autocomplete="off">
                <label>Condition text</label>
            </div>
            <div class="input-field col s6 m1 repeater-field">
                <input placeholder="3" id="repeating_count" type="text" class="validate"
                       name="repeating_count[]"
                       value="" autocomplete="off">
                <label for="repeating_count">Repeat count</label>
            </div>
            <a class="waves-effect waves-light btn right red lighten-1 removeCondition">Remove</a>
        </div>
    @endforelse
@endif

<a class="waves-effect waves-light btn" id="addCondition">Add condition</a>

<button class="waves-effect waves-light btn right" type="submit">{{ $button }}</button>