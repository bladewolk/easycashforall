@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col s12 m3 l3 xl2">
                    <div class="card">
                        <div class="card-content black-text">
                            <img style="width: 100%"
                                 src="https://corp.qiwi.com/dam/jcr:fbce4856-723e-44a2-a54f-e7b164785f01/qiwi_sign_rgb.png"
                                 alt="Qiwi logo">
                            <span class="card-title">Qiwi</span>
                        </div>
                        <div class="card-action">
                            {{ number_format($qiwi['balance']['amount'], 2) }} руб
                        </div>
                    </div>
                </div>
                <div class="col s12 m3 l3 xl2">
                    <div class="card">
                        <div class="card-content black-text">
                            <img style="width: 100%"
                                 src="http://www.finsovet.org/wp-content/uploads/2016/11/WebMoney.png"
                                 alt="Qiwi logo">
                            <span class="card-title">Webmoney</span>
                        </div>
                        <div class="card-action">
                            {{ 'NULL' }}
                        </div>
                    </div>
                </div>
                <div class="col s12 m3 l3 xl2">
                    <div class="card">
                        <div class="card-content black-text">
                            <img style="width: 100%"
                                 src="https://img.purch.com/r/520x520/aHR0cDovL3d3dy50b3B0ZW5yZXZpZXdzLmNvbS9pL3Jldi9wcm9kL2xhcmdlLzY3NjMwLXBheXBhbC1ib3guanBn"
                                 alt="Qiwi logo">
                            <span class="card-title">PayPal</span>
                        </div>
                        <div class="card-action">
                            {{ 'NULL' }}
                        </div>
                    </div>
                </div>
                <div class="col s12 m3 l3 xl2">
                    <div class="card">
                        <div class="card-content black-text">
                            <img style="width: 100%"
                                 src="http://excash24.com/wp-content/uploads/yandex.jpg"
                                 alt="Qiwi logo">
                            <span class="card-title">Яндекс деньги</span>
                        </div>
                        <div class="card-action">
                            {{ 'NULL' }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
