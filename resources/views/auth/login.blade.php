@extends('layouts.app')

@section('content')

    <div class="row login-form">
        <div class="col s12 m5 offset-m4 l2 offset-l5">
            <div class="card">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    <div class="card-content">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" class="validate"
                                       value="{{ old('email') }}" name="email">
                                <label for="email" data-error="Invalid E-Mail Address">E-Mail Address</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password" class="validate" name="password"
                                       value="{{ old('password') }}">
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="card-action text-center">
                                <p>{{ $errors->first() }}</p>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
