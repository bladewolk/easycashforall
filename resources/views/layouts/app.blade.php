<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
</head>
<body>
@Auth()
    <ul id="slide-out" class="side-nav fixed">
        <li>
            <div class="user-view">
                <div class="background">
                    <img style="width: 100%" src="https://static.pexels.com/photos/4827/nature-forest-trees-fog.jpeg">
                </div>
                <a href="{{ route('dashboard') }}"><img class="circle"
                                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSokYGFWI10uaiPaScEXZwwr_fNuACElbXgizb4_WqNh5jPmygX"></a>
                <a href="#"><span class=" name">{{ auth()->user()->name }}</span></a>
                <a href="#!email"><span class="white-text email">{{ auth()->user()->email }}</span></a>
            </div>
        </li>
        <li class="{{ in_array('tasks', request()->segments()) ? 'active ' : null }}"><a
                    class="waves-effect noUi-active" href="{{ route('tasks.index') }}">Tasks</a></li>
        <li class="{{ in_array('users', request()->segments()) ? 'active' : null }}"><a class="waves-effect"
                                                                                        href="{{ route('users') }}">Users</a>
        </li>
        <li class="{{ in_array('transactions', request()->segments()) ? 'active' : null }}"><a class="waves-effect" href="{{ route('transactions') }}">Transactions</a></li>
        <li>
            <div class="divider"></div>
        </li>
        <li><a class="waves-effect" href="/logout">Logout</a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse top-nav full hide"><i
                class="material-icons">menu</i></a>
@endauth

@yield('content')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>--}}
@yield('scripts')
@if(session()->has('success'))
    <script>
        Materialize.toast('{{ session('success') }}', 2000) // 4000 is the duration of the toast
    </script>
@endif

</body>
</html>
