<?php

use App\Models\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = [
            'image' => "https://lh3.googleusercontent.com/y4ePEkYO2mcVNDaSKTpQHOXUatDYa-cOYEMgSzU_vJkO8N4doFZt7X52hKXv7r8mUXc=w300",
            'package' => "com.arba.arba",
            'reward' => 1,
            'stars' => 1,
            'title' => "Arba",
            'expiry_date' => Carbon::now()
        ];

        $condition = [
            'type' => 'ONE_TIME',
            'text' => str_random(20)
        ];

        for ($i = 0; $i < 10; ++$i) {
            $t = Task::create($task);
            $t->conditions()->create($condition);
        }
    }
}
