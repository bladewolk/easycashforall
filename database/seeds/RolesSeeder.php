<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Roles::insert([
            [
                'name' => 'admin',
                'description' => 'admin role'
            ],
            [
                'name' => 'user',
                'description' => 'app user'
            ]
        ]);
    }
}
