<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 50)->create();
        $this->call(RolesSeeder::class);
        $this->call(AdminSeed::class);
        $this->call(TaskSeeder::class);
    }
}
