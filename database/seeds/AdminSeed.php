<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Alex',
            'email' => 'root@root.root',
            'password' => bcrypt('123qwe')
        ]);

        $admin->roles()->attach(\App\Models\Roles::whereName('admin')->first());
    }
}
