<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('response_code', 20);
            $table->string('error_code', 255)->nullable();
            $table->string('error_description', 255)->nullable();
            $table->string('status', 20);
            $table->integer('user_id')->unsigned();
            $table->float('amount');
            $table->string('payment', 20);
            $table->string('transaction_description', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
