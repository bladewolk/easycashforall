<?php

return [
    'X2' => [
        'WMID' => env('WEBMONEY_WMID', ''),
        'payer' => env('WEBMONEY_PAYER', ''),
        'key' => '',
        'password' => '',
    ]
];